### Mercredi 4 octobre 2023 à 15h30

* Lieu : Salle de réunion COALA, dans les locaux de l'équipe.
* Orateur : Amir Hosein VALIZADEH (Doctorant, Equipe COALA, LIS)
* Titre : *À venir*
* Résumé : *À venir*


### Mercredi 14 juin 2023 à 15h

* Lieu : Salle de réunion COALA, dans les locaux de l'équipe.
* Oratrice : Yousra EL-GHAZI (Doctorante, Equipe COALA, LIS)
* Titre : Conception des lignes d'un réseau de transport maritime à l'aide de la PPC
* Résumé : Le problème de conception des lignes d'un réseau de transport maritime consiste, pour un armateur, à déterminer, d'une part, quelles sont les lignes maritimes (sous forme de rotations permettant de desservir un ensemble de ports) à ouvrir, et, d'autre part, l'affectation des navires (porte-conteneurs) avec les tailles adaptées pour les différentes lignes permettant d'acheminer tous les flux de conteneurs. Dans cet exposé, nous proposons une modélisation de ce problème à l'aide de la programmation par contraintes. Puis, nous présentons une étude préliminaire de sa résolution à l'aide de solveurs de l'état de l'art

### Mercredi 5 avril 2023 à 10h

* Lieu : salle de réunion COALA, dans les locaux de l'équipe.
* Orateur : Richard OSTROWSKI (Equipe COALA, LIS)
* Titre : Nouvel encodage SAT des problèmes de Packing Orthogonal
* Résumé : D'années en années, les solveurs se montrent de plus en plus efficaces pour la résolution pratique du problème SAT. Cependant, la capacité des solveurs à résoudre divers types de problèmes, à fermer certains problèmes mathématiques jusque-là restés ouverts (boolean pythagorian triples) est fortement lié à la manière dont sont modélisés ces problèmes. Dans les années 1997, 10 challenges ont été proposés. Les fameux problèmes 32 bits parity (parity-32) résistaient toujours et des solveurs dédiés ont vu le jour dans les années 2000 (lsat, eqsatz,march). Par la suite, des travaux sur une représentation efficace des contraintes de cardinalités, a permis une meilleure réécriture de ces problèmes et de les résoudre avec les solveurs de l'époque. De manière générale, différents codages existent pour modéliser en SAT (codage direct, codage des supports, codage logarithmique). Depuis 2011, l'order encoding, semble être le plus efficace. Un encodage doit offrir un bon compromis entre la propagation des contraintes et la compacité de la formule. Dans cet exposé, je m'intéresse aux problèmes de packing (OPP). Une idée intéressante, pour la résolution, est de considérer le problème sous la forme de graphes d'intervalles. Cette modélisation offre l'avantage de supprimer de nombreuses symétries de placement. À l'époque, S. Grandcolas et C. Pinto ont proposé une formulation pour SAT. Cependant, cette modélisation comportait 2 inconvénients : (1) il fallait modéliser, sous la forme de contraintes, la notion de stables i-faisables (exponentiel dans certains cas) et (2) de nombreuses configurations sont explorées alors qu'elles ne correspondent pas à la réalité. Une nouvelle reformulation sera proposée et des premiers résultats expérimentaux encourageants seront présentés.
